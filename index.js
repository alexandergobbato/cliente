const express = require('express');
const app = express();
const port = 8000;

//habilitar servidor
app.listen(port,()=>{
    console.log("Projeto executando na porta: " + port);
});

//recurso de consulta
app.get('/funcionario',(req, res)=>{
    console.log("Acessando o recurso FUNCIONARIO");
    res.send("{message:/get funcionario}");
});

app.get('/pesquisa',(req, res)=>{
    let dados = req.query;
    res.send(dados.nome + " " + dados.sobrenome);
});

app.get('/pesquisa/cliente/:codigo',(req, res)=>{
    let id = req.params.codigo;
    res.send("Dados do cliente " + id);
});

const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
app.post('/funcionario/gravar',(req, res)=>{
    let valores = req.body;
    console.log("body: " + req);
    console.log("req.body: " + req.body);
    console.log("valores: " + valores);
    console.log("Sobrenome: " + valores.sobrenome);
    console.log("Idade: "+ valores.idade);
    res.send("sucesso");
});

//versao 17.6.0
//versao 12.16.2